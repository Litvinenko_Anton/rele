
//#include <OneButton.h>

//#include <OLED_I2C.h>
//OLED  myOLED(SDA, SCL, 8); // инициализация OLED дисплея
//extern uint8_t SmallFont[];

#include <U8g2lib.h>
U8G2_SSD1306_128X32_UNIVISION_F_SW_I2C u8g2(U8G2_R0, /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);   // Adafruit Feather ESP8266/32u4 Boards + FeatherWing OLED

void pintu8g2() {
  u8g2.clearBuffer();          // clear the internal memory
  u8g2.setFont(u8g2_font_ncenB08_tr); // choose a suitable font
  u8g2.drawStr(0,10,"Hello World!");  // write something to the internal memory
  u8g2.sendBuffer();          // transfer internal memory to the display
  delay(1000);  
  
}

//#define CLK 2
//#define DIO 3
//#include "GyverTM1637.h"
//GyverTM1637 disp(CLK, DIO);

// Constants
const int BUTTON_1_PIN = 2;
const int BUTTON_2_PIN = 3;
const int RELE_PIN =  4;
const int LED_PIN =  13;

const unsigned long DELAY_TIME_MIN = 60000;
const unsigned long DELAY_TIME_MIN_5 = 300000;
const unsigned long DELAY_TIME_MIN_10 = 600000;
const unsigned long DELAY_TIME_MIN_15 = 900000;
const unsigned long DELAY_TIME_MIN_30 = 1800000;

const int MIN_COUNT =  1;
const int MAX_COUNT =  6;
const unsigned long delayTime = DELAY_TIME_MIN_10;
int buttonCliclCount = MIN_COUNT;


//OneButton button1(BUTTON_1_PIN, true);
//OneButton button2(BUTTON_2_PIN, true);


// variables will change:
int buttonState = LOW;         // variable for reading the pushbutton status
bool buttonFlag = false;
bool releState = false;
unsigned long startTime;
unsigned long releStartTime;

void setup() {
  Serial.begin(9600);
  startTime = millis();
  
  initPins();
//  initBbuttons();
//  initOLED();

  turnOff();
}

void initPins() {
  pinMode(BUTTON_1_PIN, INPUT_PULLUP); // initialize the pushbutton pin as an input
  pinMode(BUTTON_2_PIN, INPUT_PULLUP); // initialize the pushbutton pin as an input
  pinMode(RELE_PIN, OUTPUT); // initialize the RELE pin as an output
  pinMode(LED_PIN, OUTPUT); // initialize the LED pin as an output
}

//void initBbuttons() {
//  //  attachInterrupt(0, buttonInterrupt, FALLING);
//  Serial.println("Starting TwoButtons...");
//  button1.attachClick(clickButton);
//  button1.attachDoubleClick(doubleClickButton);
//  button1.attachLongPressStart(longPressButton);
//}

//void initOLED() {
//  myOLED.begin();            // инициализация OLED дисплея
//  myOLED.setFont(SmallFont); // инициализация шрифта
//}

//void buttonInterrupt() {
//  ++buttonCliclCount;
//  buttonState = HIGH;
//}


void loop() {
//  button1.tick();
  timerCheckStop(delayTime);
//  printOLED(25.5, 50.5, 75.5);
  pintu8g2();
}

//const int topT = 5;
//const int topH = 15;
//const int topP = 25;
//const int startNum = 32;
//const int startChar = 64;

void printOLED(float temp, float humidity, float pressure) {

//  myOLED.print("Temp: ", 0, topT);
//  myOLED.printNumF(temp, 2, startNum, topT);
//  myOLED.print("C", startChar, topT);
//
//  myOLED.print("Hum: ", 0, topH);
//  myOLED.printNumF(humidity, 2, startNum, topH);
//  myOLED.print("%", startChar, topH);
//
//  myOLED.print("Pres: ", 0, topP);
//  myOLED.printNumF(pressure, 2, startNum, topP);
//  myOLED.print("hPa", (startChar + 12), topP);
//
//  myOLED.update();

}

void clickButton() { // ADD ONE COUNT TO TIMER
  buttonFlag = true;
  ++buttonCliclCount;
  if (buttonCliclCount > MAX_COUNT)
    buttonCliclCount = MIN_COUNT;
  printSerial();
}

void doubleClickButton() { // START TIMER
  buttonFlag = true;
  turnOn();
}

void longPressButton() { // SKIP TIMER
  turnOff();
}

void turnOn() {
  if (!releState) {
    digitalWrite(LED_PIN, HIGH);// turn LED on
    digitalWrite(RELE_PIN, LOW);// turn RELE on
    timerStart();
    printSerial();
  }
}

void turnOff() {
  digitalWrite(LED_PIN, LOW);// turn LED off
  digitalWrite(RELE_PIN, HIGH);// turn RELE off
  timerStop();
  printSerial();
}

void timerStart() {
  if (!releState) {
    releState = true;
    releStartTime = millis();
  }
}

void timerStop() {
  releState = false;
  buttonCliclCount = 1;
  buttonFlag = false;
}

void timerCheckStop(unsigned long delayTimeMillis) {
  if (releState && (millis() - releStartTime) > (delayTimeMillis * buttonCliclCount)) {
    turnOff();
  }
}


void printSerial() {
  Serial.print("buttonState = ");
  Serial.print(buttonState == HIGH);
  Serial.println(" (HIGH)");

  Serial.print("releState = ");
  Serial.println(releState);

  Serial.print("millis() = ");
  Serial.println(millis());

  Serial.print("millis() - releStartTime = ");
  Serial.print((millis() - releStartTime));
  Serial.println(" millis");

  Serial.print("timerCheckStop(debugTime) - ");
  Serial.println((millis() - releStartTime) > (delayTime * buttonCliclCount));

  Serial.print("buttonCliclCount = ");
  Serial.println(buttonCliclCount);

  Serial.println("");
}
